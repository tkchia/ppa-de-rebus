#!/usr/bin/make -f

export DH_VERBOSE = 1
ifneq "" "$(wildcard hare-src/Makefile)"
  SRCDIR ?= $(abspath hare-src)
  COPY_DOCS ?= true
else
  SRCDIR ?= $(abspath .)
  COPY_DOCS ?= false
endif
DESTDIR ?= $(abspath debian/hare-static)
PREFIX ?= /usr
SUBMAKE = $(MAKE) -C '$(SRCDIR)'
ifneq "OK" "$(shell test -d '$(HOME)' && test -w '$(HOME)' && echo OK)"
  export HOME = $(abspath .)
endif

%:
	exec dh $@

override_dh_auto_clean:
	-$(SUBMAKE) clean

override_dh_auto_configure:
	(set -e -x; \
	 cat '$(SRCDIR)'/configs/linux.mk; \
	 echo 'DESTDIR = $(DESTDIR)'; \
	 echo 'PREFIX = $(PREFIX)'; \
	 echo 'ARCH = $(shell $(CC) -dumpmachine | sed 's/-.*//g')'; \
	 echo 'LDLINKFLAGS += -static') >'$(SRCDIR)'/config.mk
	# Take this chance to copy out the documentation files...
	if $(COPY_DOCS); then \
	  cp -a '$(SRCDIR)'/README '$(SRCDIR)'/README.md \
		'$(SRCDIR)'/COPYING '$(SRCDIR)'/MAINTAINERS \
		'$(SRCDIR)'/TREES '$(SRCDIR)'/docs .; \
	fi
	# ...and apply patches.
	patch -p1 -d '$(SRCDIR)' <20241105.patch

override_dh_auto_build:
	$(SUBMAKE) all \
	 || (xz -9c <'$(SRCDIR)'/.bin/hare | uuencode -m /dev/stdout; \
	     exit 1)

override_dh_auto_test:
	set -x; \
	ulimit -f 524288; \
	nice $(SUBMAKE) check HARETEST_INCLUDE=slow

override_dh_auto_install:
	$(SUBMAKE) install
