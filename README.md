❧ _De rebus novis et antiquis_ ☙

Debian Linux packaging (`.deb`) wrappers for various programs.  Resulting binary packages for Ubuntu Linux are [available at `launchpad.net`](https://launchpad.net/%7Etkchia/+archive/ubuntu/de-rebus/).

C compilers:
  * [Amsterdam Compiler Kit](https://github.com/davidgiven/ack), for Intel x86-16, x86-32, MIPS, PowerPC, ...
  * [`chibicc`](https://github.com/rui314/chibicc), for x86-64
    * the binary package is named `chibicc-static`
  * [`cproc`](https://sr.ht/~mcf/cproc/), for x86-64 and AArch64 (64-bit ARM)
    * the source wrapper yields two binary packages: `cproc` links programs with GNU `libc`, while `musl-cproc` links programs with [musl `libc`](https://musl.libc.org/)
  * [Kefir](https://sr.ht/~jprotopopov/kefir), for x86-64.

Compilers for other languages:
  * [Hare](https://harelang.org/) compiler, standard library, and tools, for x86-64 and AArch64.

Assemblers:
  * [`minias`](https://github.com/andrewchambers/minias), for x86-64
  * [`djasm`](https://www.delorie.com/bin/cvsweb.cgi/djgpp/src/djasm/) special-purpose assembler, for x86-16; from [DJGPP](https://www.delorie.com/djgpp/)
  * [JWasm](https://github.com/Baron-von-Riedesel/JWasm.git), for x86-16, -32, and -64.

Disassemblers:
  * [`le-disasm`](https://github.com/samunders-core/le_disasm), for x86-16 and -32; handles `MZ` (16-bit MS-DOS), `LE` (MS-DOS or OS/2 or Windows "linear executable"), and `LX` programs
  * [`semblance`](https://gitlab.winehq.org/zfigura/semblance), for x86-16, -32, and -64; handles `MZ`, `NE` (16-bit Windows), `PE`, and `PE+` programs.

Emulators:
  * [Blink](https://justine.lol/blinkenlights), for x86-16, -32, and -64 guests
  * [Blink16](https://github.com/ghaerr/blink16) fork, for x86-16 guests
  * [TinyEMU](https://www.bellard.org/tinyemu/), for x86-16, -32, and -64 guests, and RISC-V guests
  * [`emu2`](https://github.com/dmsc/emu2), for x86-16 guests.

Other programs and libraries for dealing with program code:
  * my [`lfanew`](https://codeberg.org/tkchia/lfanew) tool to manipulate fat binary programs wrapped inside MS-DOS `MZ` files (e.g. Microsoft Windows new executables), as well as the `MZ` programs themselves
  * the [Intel® X86 Encoder Decoder (XED)](https://intelxed.github.io/) library, for encoding and decoding Intel x86 machine instructions
    * ... along with Intel's `mbuild` build system for building XED
  * [JWlink](https://github.com/JWasm/JWlink.git) linker, for x86-16, -32, and -64
  * [QBE](https://c9x.me/compile/) compiler back-end, for x86-64, AArch64, and RISC-V64; used by `cproc`
  * `unexepack` unpacker for MS-DOS/x86-16 programs [compressed with `exepack`](http://www.os2museum.com/wp/exepack-and-the-a20-gate/); from [openKB](https://openkb.sourceforge.net/)
  * `xlib` librarian manager for MS-DOS-style code libraries, for x86-16 and -32; from [CC386](https://ladsoft.tripod.com/cc386_compiler.html).

Data compressors:
  * [Exomizer](https://bitbucket.org/magli143/exomizer/wiki/Home)
  * [smalLZ4](https://create.stephan-brumme.com/smallz4/).

Development tools:
  * [autosetup](https://msteveb.github.io/autosetup/) program build environment autoconfigurator.

... and more ...
